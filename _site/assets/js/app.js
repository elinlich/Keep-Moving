// This example uses ES6 (ECMAScript 2015), a more recent version of JavaScript.
$(function () {
    console.log("Hello from jQuery");
    var $body = $("body"); // Body element (use $ in variable name to show it's a jQuery object)

    /**
     * Screenshot slider.
     */
    var $slidesButtons = $(".slides-button");
    var $slidesItems = $(".slides-item");
    var $screenItems = $(".screen-item");
    $slidesButtons
        .on("click", function () {
            var $activeSlidesButton = $(this);
            var index = $activeSlidesButton.index();

            $slidesButtons
                .addClass("btn-secondary")
                .removeClass("btn-primary");

            $activeSlidesButton
                .addClass("btn-primary")
                .removeClass("btn-secondary");

            $slidesItems
                .hide()
                .eq(index)
                    .show();

            $screenItems
                .hide()
                .eq(index)
                    .show();
            
        })
        .first()
            .trigger("click");

    /*
     * Scroll-to-top button.
     */
    // Variables to hold objects and values that are generated, but will be used more than once.
    var $scrollToTopButton = $("#scroll-to-top"); // Button (use $ in variable name to show it's a jQuery object).
    var scrollTreshold = window.innerHeight / 3; // Scroll distance treshold when to show button. Value is 1/3 of window height. 
    
    // Show or hide the scroll button depending on scroll distance.
    $(window)
        .on("scroll", function (event) {
            if ($body.scrollTop() < scrollTreshold) {
                $scrollToTopButton.hide();
            } else {
                $scrollToTopButton.show();
            }
        });

    // Scroll to top when button is clicked.
    $scrollToTopButton
        .on("click", function (event) {
            $body.animate({
                scrollTop: 0
            });
        })

    /*
     * Counter
     */
    $("[data-counter-start][data-counter-increment]")
        .each(function () {
            var locale = $("html").attr("lang");
            var $counter = $(this);
            var count = parseInt($counter.data("counter-start"));
            var increment = parseInt($counter.data("counter-increment"));
            var interval = 500; // In milliseconds.

            $counter
                .css({
                    fontFamily: "monospace"
                })
                .text(count.toLocaleString());

            setInterval(function () {
                count = count + increment;
                $counter.text(count.toLocaleString());
            }, interval);
            
        });

    /*
     * Contact form using progressive disclosure pattern.
     */
    var $contactForm = $("#contact-form");
    var $submitButton = $("#contact-form").find("[type=submit]");

    $contactForm
        .on("keyup", function (event) {
            if (this.checkValidity()) {
                $submitButton.prop("disabled", false);
            } else {
                $submitButton.prop("disabled", true);
            }
        })
        .children('.row')
            .not(':first, :last')
                .hide()
                .end()
            .end()
        .find("[name]")
            .on("keyup blur change", function (event) {
                $formField = $(this);
                if (this.validationMessage.length <= 0) {
                    // Valid form
                    $formField
                        .addClass("form-control-success")
                        .removeClass("form-control-warning")
                        .parents(".row")
                            .addClass("has-success")
                            .removeClass("has-warning")
                            .find(".form-control-feedback")
                                .text("")
                                .end()
                            .next()
                                .show();
                } else {
                    // Invalid form
                    $formField
                        .addClass("form-control-warning")
                        .removeClass("form-control-success")
                        .parents(".row")
                            .addClass("has-warning")
                            .removeClass("has-success")
                            .find(".form-control-feedback")
                                .text(this.validationMessage);
                }
            })

    $submitButton
        .prop('disabled', true) // Disable by default.
        .on("click", function (event) {
            event.preventDefault(); // Prevent the button from submitting the form.
            $(this)
                .prop("disabled", true)
                .find("i")
                    .addClass("fa-refresh fa-spin")
                    .removeClass("fa-paper-plane");
        });

    /**
     * Picture Slider
     */
    $(".c-picture-viewer")
        .each(function () {
            var $pictureViewer = $(this);
            var $pictures = $pictureViewer.find(".o-picture");
            var $thumb = $pictureViewer.find(".o-thumb");
            var pictureViewerWidth = $pictureViewer.width();
            var pictureWidth = 450;
            var pictureScaleLarge = 1/3;
            var pictureScaleSmall = 1/5;
            var pictureGutter = Math.floor((pictureViewerWidth - (($pictures.length - 1) * pictureScaleSmall + pictureScaleLarge) * pictureWidth) / ($pictures.length - 1));
            var pictureWidthSmall = Math.floor(pictureWidth * pictureScaleSmall);
            $pictures
                .click(function (event) {
                    var $picture = $(this);
                    $picture.addClass("is-active").siblings().removeClass("is-active");
                    $thumb.css({
                        left: (pictureWidthSmall + pictureGutter) * $picture.index() + "px"
                    });
                })
                .eq(2)
                    .trigger("click");
        });

});
 
/**
 * navbar bolletjes
 */
$("nav a").click(function() {

   // Remove the active class from the element that is currently active
   $(".active").removeClass("active");  

   // Add the active class to the element that was just clicked
   $(this).addClass("active");

   return false;
});

