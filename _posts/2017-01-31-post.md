---
layout: post
title: Hello world!
date: 2017-01-31
excerpt_separator: <!--more-->
---
<div class="container">
<h4>{{ page.title}}</h4>

<div class="left">
<div class="datum">
<p><i class="fa fa-calendar-o" aria-hidden="true"></i> {{ page.date | date: '%B %d, %Y'}}</p>
</div>

<p>
Welkom op onze site, lees nu onze eerste blogberichten. <!--more--></p>
<p>In deze blog houden wij jullie op de hoogte van wat er allemaal gebeurt in en rond Keep Moving. Dit zal gaan van leuke weetjes naar interessante informatie tot updates binnen onze app. Wil je zeker zijn dat je geen enkel bericht mist? Schrijf je dan in voor onze nieuwsbrief. </p> 
<p>
Lijkt deze app je wel interessant en heb je hem nog niet gedownload? Twijfel dan zeker niet om dat te doen! Zo maken we samen de wereld weer een beetje fitter. En vergeet het niet: Keep Moving want te druk is geen uitvlucht meer. 
</p>

</div>
</div>
